<%@ page import="taller3constraints.Forum" %>



<div class="fieldcontain ${hasErrors(bean: forumInstance, field: 'admin', 'error')} ">
	<label for="admin">
		<g:message code="forum.admin.label" default="Admin" />
		
	</label>
	
<ul class="one-to-many">
<g:each in="${forumInstance?.admin?}" var="a">
    <li><g:link controller="admin" action="show" id="${a.id}">${a?.encodeAsHTML()}</g:link></li>
</g:each>
<li class="add">
<g:link controller="admin" action="create" params="['forum.id': forumInstance?.id]">${message(code: 'default.add.label', args: [message(code: 'admin.label', default: 'Admin')])}</g:link>
</li>
</ul>


</div>

<div class="fieldcontain ${hasErrors(bean: forumInstance, field: 'category', 'error')} required">
	<label for="category">
		<g:message code="forum.category.label" default="Category" />
		<span class="required-indicator">*</span>
	</label>
	<g:textField name="category" required="" value="${forumInstance?.category}"/>

</div>

<div class="fieldcontain ${hasErrors(bean: forumInstance, field: 'name', 'error')} required">
	<label for="name">
		<g:message code="forum.name.label" default="Name" />
		<span class="required-indicator">*</span>
	</label>
	<g:textField name="name" required="" value="${forumInstance?.name}"/>

</div>

<div class="fieldcontain ${hasErrors(bean: forumInstance, field: 'post', 'error')} required">
	<label for="post">
		<g:message code="forum.post.label" default="Post" />
		<span class="required-indicator">*</span>
	</label>
	<g:select id="post" name="post.id" from="${taller3constraints.Post.list()}" optionKey="id" required="" value="${forumInstance?.post?.id}" class="many-to-one"/>

</div>

