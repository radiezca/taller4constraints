package taller3constraints

class Post {

	String topic
	Date dateCreated
	Date lastUpdate
	boolean	itsAllowed

	static hasMany = [regular:Regular]
	static hasONe = [file:File]

	static constraints = {
		topic nullable:false, size:3..50
		dateCreated nullable:false, min: new Date()
		lastUpdate nullable:false, min: new Date()
		itsAllowed nullable:false
	}
}
