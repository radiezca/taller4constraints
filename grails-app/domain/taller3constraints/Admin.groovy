package taller3constraints

class Admin extends User{
	int level
	double rating

	static belongsTo = [user:User]
	static hasOne = [forum:Forum]

	static constraints = {
		level nullable: false, range: 1..5
		rating nullable: false, range: 0..100
	}
}
