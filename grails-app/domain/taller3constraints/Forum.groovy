package taller3constraints

class Forum {
	String name
	Date dateCreated
	String category

	static hasMany = [admin:Admin]
	static hasOne = [post:Post]

	static constraints = {
		name nullable: false, size: 3..20, unique: true
		dateCreated nullable: false, min: new Date()
		category nullable: false, size:3..15
	}
}
