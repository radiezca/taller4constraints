package taller3constraints

class User {

	String name
	String lastName
	int age
	String userName
	String password


	static constraints = {
		name blank: false, nullable: false, size:3..50
		lastName blank: false, nullable: false, size:3..50
		age blank: false, nullable: false, min: 13
		userName blank: false, nullable: false, matches: "[a-z]*[A-Z0-9]+[a-z]*", minSize: 8
		password blank: false, nullable: false, unique:true
	}
}
