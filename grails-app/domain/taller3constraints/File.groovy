package taller3constraints

class File {
	String fileType
	Byte[]	content
	double	size

	static hasMany = [post:Post]

	static constraints = {
		fileType blank:false, nullable:false, matches:"[a-zA-Z]*/[a-zA-Z]*"
		content blank:false, nullable:false
		size blank:false, nullable:false, max: 1310720
	}
}
